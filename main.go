package main

import (
	"fmt"

	"github.com/arraytocollection/pkg/phpclass"
	"github.com/arraytocollection/pkg/renderer"
)

func main() {
	phpClass := phpclass.CreateClass(phpclass.GetClassName())
	for {
		if phpClass.GetAdditionalProperty() == false {
			break
		}
	}
	renderer.SaveToFile(phpClass)
	fmt.Println("Saved to file.")
}
