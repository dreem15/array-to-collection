package phpinterface

import (
	"github.com/arraytocollection/pkg/phpclass"
)

type PhpInterface struct {
	Name  string
	Class *phpclass.PhpClass
}
