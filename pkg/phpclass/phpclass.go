package phpclass

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type PhpClass struct {
	Name       string
	Properties []PhpClassProperty
}

type PhpClassProperty struct {
	Name        string
	Type        string
	Constructor bool
}

func GetClassName() string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("How class should be called?")
	className, _ := reader.ReadString('\n')
	return strings.TrimRight(className, "\n")
}

func CreateClass(className string) *PhpClass {
	return &PhpClass{
		Name:       className,
		Properties: nil,
	}
}

func (phpClass *PhpClass) AddPropertyToClass(propertyName string, propertyType string, constructor bool) {
	phpClass.Properties = append(phpClass.Properties, PhpClassProperty{
		Name:        propertyName,
		Type:        propertyType,
		Constructor: constructor,
	})
}

func (phpClass *PhpClass) GetAdditionalProperty() bool {
	reader := bufio.NewReader(os.Stdin)

	fmt.Println("Property name?")
	propertyName, _ := reader.ReadString('\n')
	propertyName = strings.TrimRight(propertyName, "\n")

	// fast way to add property example: firstName,string,n | name,type,addToConstructor
	if strings.Contains(propertyName, ",") {
		data := strings.Split(propertyName, ",")
		phpClass.AddPropertyToClass(data[0], data[1], data[2] == "y")
		fmt.Println("Add another property? [y/n]")
		accepted, _ := reader.ReadString('\n')
		return strings.TrimRight(accepted, "\n") == "y"
	}

	fmt.Println("Property type?")
	propertyType, _ := reader.ReadString('\n')
	propertyType = strings.TrimRight(propertyType, "\n")

	fmt.Println("Place in constructor? [y/n]")
	constructor, _ := reader.ReadString('\n')

	phpClass.AddPropertyToClass(propertyName, propertyType, strings.TrimRight(constructor, "\n") == "y")

	fmt.Println("Add another property? [y/n]")
	accepted, _ := reader.ReadString('\n')
	return strings.TrimRight(accepted, "\n") == "y"
}
