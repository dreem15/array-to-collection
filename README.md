# todo
- web interface (?)
- hydrator/extractor
- mapper
- collections
- refactor by responsibility (render only renders stuff etc.)

# short usage
```
./main 
How class should be called?
Test
Property name?
first,string,n
Add another property? [y/n]
y
Property name?
second,string,n
Add another property? [y/n]
n
Saved to file.
```

# standard usage
```
./main 
How class should be called?
Order
Property name?
amount
Property type?
int
Place in constructor? [y/n]
y
Add another property? [y/n]
y  
Property name?
tax
Property type?
int
Place in constructor? [y/n]
n
Add another property? [y/n]
y
Property name?
firstName
Property type?
string
Place in constructor? [y/n]
n
Add another property? [y/n]
n
Saved to file.
```
