package renderer

import (
	"fmt"
	"os"
	"strings"

	"github.com/arraytocollection/pkg/phpclass"
	"github.com/arraytocollection/pkg/phpinterface"
)

const phpTag = "<?php\n"
const strictTypesTag = "declare(strict_types=1);\n\n"
const namespaceTag = "namespace ChangeMePlease;\n\n"

const classOpenTag = "class %s implements %sInterface \n{"
const interfaceOpenTag = "interface %s \n{"
const memberAttributeAnnotationTag = "\n\t/**\n\t * @var %s\n\t */"
const memberAttributeTag = "\n\tprivate $%s;\n"

const annotationOpenTag = "\n\t/**"
const constructorAnnotationTag = "\n\t * @param %s $%s"
const annotationCloseTag = "\n\t */"

const constructorOpenTag = "\n\tpublic function __construct("
const constructorInputTag = "\n\t\t %s $%s%s"
const constructorAfterInputBraceTag = "\n\t) {"
const constructorBodyTag = "\n\t\t $this->%s = $%s;"
const constructorCloseTag = "\n\t}\n"

const getterAnnotationTag = "\n\t/**\n\t * @return %s\n\t */"
const getterBodyTag = "\n\tpublic function get%s(): %s\n\t{\n\t\treturn $this->%s;\n\t}\n"
const getterInterfaceBodyTag = "\n\tpublic function get%s(): %s;\n"

const nullableGetterAnnotationTag = "\n\t/**\n\t * @throws Empty%sException\n\t * @return %s\n\t */"
const nullableGetterBodyTag = "\n\tpublic function get%s(): %s\n\t{\n\t\tif (!is_object($this->%s)) {\n\t\t\tthrow new Empty%sException();\n\t\t}\n\t\treturn $this->%s;\n\t}\n"

const setterAnnotationTag = "\n\t/**\n\t * @return void\n\t */"
const setterBodyTag = "\n\tpublic function set%s(%s $%s): void\n\t{\n\t\t$this->%s = $%s;\n\t}\n"
const setterInterfaceBodyTag = "\n\tpublic function set%s(%s $%s): void;\n"

const classEndingTag = "\n}\n"

type PhpFile struct {
	Source *os.File
}

func OpenFileStreamForClass(phpClass *phpclass.PhpClass) *PhpFile {
	os.Mkdir(fmt.Sprintf("output/%s", phpClass.Name), os.ModePerm)
	os.Mkdir(fmt.Sprintf("output/%s/Exception", phpClass.Name), os.ModePerm)

	file, err := os.Create(fmt.Sprintf("output/%s/%s.php", phpClass.Name, phpClass.Name))
	if err != nil {
		panic(err)
	}
	return &PhpFile{Source: file}
}

func (phpFile *PhpFile) AppendHeaders(phpClass *phpclass.PhpClass) {
	phpFile.Source.WriteString(phpTag)
	phpFile.Source.WriteString(strictTypesTag)
	phpFile.Source.WriteString(namespaceTag)
	phpFile.Source.WriteString(fmt.Sprintf(classOpenTag, phpClass.Name, phpClass.Name))
}

func (phpFile *PhpFile) AppendInterfaceHeaders(phpInterface *phpinterface.PhpInterface) {
	phpFile.Source.WriteString(phpTag)
	phpFile.Source.WriteString(strictTypesTag)
	phpFile.Source.WriteString(namespaceTag)
	phpFile.Source.WriteString(fmt.Sprintf(interfaceOpenTag, phpInterface.Name))
}

func (phpFile *PhpFile) AppendAttributes(phpClass *phpclass.PhpClass) {
	for _, property := range phpClass.Properties {
		phpFile.Source.WriteString(fmt.Sprintf(memberAttributeAnnotationTag, property.Type))
		phpFile.Source.WriteString(fmt.Sprintf(memberAttributeTag, property.Name))
	}
}

func (phpFile *PhpFile) AppendConstructor(phpClass *phpclass.PhpClass) {
	var constructorProps []phpclass.PhpClassProperty
	for _, property := range phpClass.Properties {
		if property.Constructor == true {
			constructorProps = append(constructorProps, property)
		}
	}

	if len(constructorProps) == 0 {
		return
	}

	phpFile.Source.WriteString(annotationOpenTag)
	for _, property := range constructorProps {
		phpFile.Source.WriteString(fmt.Sprintf(constructorAnnotationTag, property.Type, property.Name))
	}
	phpFile.Source.WriteString(annotationCloseTag)

	phpFile.Source.WriteString(constructorOpenTag)
	for index, property := range constructorProps {
		var comma = ","
		if len(constructorProps)-1 == index {
			comma = ""
		}
		phpFile.Source.WriteString(fmt.Sprintf(constructorInputTag, property.Type, property.Name, comma))
	}
	phpFile.Source.WriteString(constructorAfterInputBraceTag)
	for _, property := range constructorProps {
		phpFile.Source.WriteString(fmt.Sprintf(constructorBodyTag, property.Name, property.Name))
	}
	phpFile.Source.WriteString(constructorCloseTag)
}

func (phpFile *PhpFile) AppendGetterAndSetters(phpClass *phpclass.PhpClass) {
	for _, property := range phpClass.Properties {
		if property.Constructor == false {
			phpFile.Source.WriteString(fmt.Sprintf(nullableGetterAnnotationTag, strings.Title(property.Name), property.Type))
			phpFile.Source.WriteString(fmt.Sprintf(nullableGetterBodyTag, strings.Title(property.Name), property.Type, property.Name, strings.Title(property.Name), property.Name))
			phpFile.Source.WriteString(setterAnnotationTag)
			phpFile.Source.WriteString(fmt.Sprintf(setterBodyTag, strings.Title(property.Name), property.Type, property.Name, property.Name, property.Name))
			CreateException(phpClass.Name, fmt.Sprintf("Empty%sException", strings.Title(property.Name)))
		} else {
			phpFile.Source.WriteString(fmt.Sprintf(getterAnnotationTag, property.Type))
			phpFile.Source.WriteString(fmt.Sprintf(getterBodyTag, strings.Title(property.Name), property.Type, property.Name))
		}
	}
	phpFile.Source.WriteString(classEndingTag)
}

func (phpFile *PhpFile) AppendInterfaceGetterAndSetters(phpInterface *phpinterface.PhpInterface) {
	for _, property := range phpInterface.Class.Properties {
		if property.Constructor == false {
			phpFile.Source.WriteString(fmt.Sprintf(nullableGetterAnnotationTag, strings.Title(property.Name), property.Type))
			phpFile.Source.WriteString(fmt.Sprintf(getterInterfaceBodyTag, strings.Title(property.Name), property.Type))
			phpFile.Source.WriteString(setterAnnotationTag)
			phpFile.Source.WriteString(fmt.Sprintf(setterInterfaceBodyTag, strings.Title(property.Name), property.Type, property.Name))
		} else {
			phpFile.Source.WriteString(fmt.Sprintf(getterAnnotationTag, property.Type))
			phpFile.Source.WriteString(fmt.Sprintf(getterInterfaceBodyTag, strings.Title(property.Name), property.Type))
		}
	}
	phpFile.Source.WriteString(classEndingTag)
}

func CreateException(phpClassName string, exceptionName string) {
	file, err := os.Create(fmt.Sprintf("output/%s/Exception/%s.php", phpClassName, exceptionName))
	if err != nil {
		panic(err)
	}
	defer file.Close()

	file.WriteString(phpTag)
	file.WriteString(strictTypesTag)
	file.WriteString("namespace ChangeMePlease\\Exception;\n\n")
	file.WriteString(fmt.Sprintf("class %s extends \\Exception \n{\n\n}\n", exceptionName))

	file.Sync()
}

func CreateInterfaceFromClass(phpClass *phpclass.PhpClass) *phpinterface.PhpInterface {
	return &phpinterface.PhpInterface{phpClass.Name + "Interface", phpClass}
}

func OpenFileStreamForInterface(phpInterface *phpinterface.PhpInterface) *PhpFile {
	file, err := os.Create(fmt.Sprintf("output/%s/%s.php", phpInterface.Class.Name, phpInterface.Name))
	if err != nil {
		panic(err)
	}
	return &PhpFile{Source: file}
}

func SaveToFile(phpClass *phpclass.PhpClass) bool {
	phpFile := OpenFileStreamForClass(phpClass)
	defer phpFile.Source.Close()

	phpFile.AppendHeaders(phpClass)
	phpFile.AppendAttributes(phpClass)
	phpFile.AppendConstructor(phpClass)
	phpFile.AppendGetterAndSetters(phpClass)

	err := phpFile.Source.Sync()
	if err != nil {
		return false
	}

	phpInterface := CreateInterfaceFromClass(phpClass)
	interfaceFile := OpenFileStreamForInterface(phpInterface)

	interfaceFile.AppendInterfaceHeaders(phpInterface)
	interfaceFile.AppendInterfaceGetterAndSetters(phpInterface)

	err = interfaceFile.Source.Sync()
	if err != nil {
		return false
	}

	return true
}
